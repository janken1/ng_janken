import { Injectable, Inject } from '@angular/core';
import { InternalStorage, LoopBackConfig } from '../sdk';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JankenService {
  private lb_server =
    LoopBackConfig.getPath() + '/' + LoopBackConfig.getApiVersion() + '/';
  protected prefix = 'jaken_';

  constructor(
    @Inject(InternalStorage) protected storage: InternalStorage,
    public http: HttpClient
  ) {}

  getValue(key) {
    const valor = this.tryJSON(this.load(key));
    return valor ? valor : this.load(key);
  }

  setValue(key, value) {
    this.persist(key, value);
  }

  protected load(prop: string): any {
    return this.storage.get(`${this.prefix}${prop}`);
  }
  /**
   * @method clear
   * @description
   * This method will clear cookies or the local storage.
   **/
  public clear(): void {
    // Object.keys(this.token).forEach((prop: string) => this.storage.remove(`${this.prefix}${prop}`));
    // this.token = new SDKToken();
  }
  /**
   * @method persist
   * @description
   * This method saves values to storage
   **/
  protected persist(prop: string, value: any, expires?: Date): void {
    try {
      this.storage.set(
        `${this.prefix}${prop}`,
        typeof value === 'object' ? JSON.stringify(value) : value,
        null
      );
    } catch (err) {
      console.log('Cannot access local/session storage:', err);
    }
  }
  tryJSON(valor) {
    try {
      return JSON.parse(valor);
    } catch (error) {
      return valor;
    }
  }
  testJSON(valor) {
    try {
      return JSON.parse(valor);
    } catch (error) {
      return null;
    }
  }
}
