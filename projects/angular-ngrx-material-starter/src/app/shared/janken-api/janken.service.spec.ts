import { TestBed } from '@angular/core/testing';

import { JankenService } from './janken.service';

describe('JankenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JankenService = TestBed.get(JankenService);
    expect(service).toBeTruthy();
  });
});
