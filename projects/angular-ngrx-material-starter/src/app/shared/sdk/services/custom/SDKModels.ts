/* tslint:disable */
import { Injectable } from '@angular/core';
import { Player } from '../../models/Player';
import { Rule } from '../../models/Rule';
import { Game } from '../../models/Game';

export interface Models {
  [name: string]: any;
}

@Injectable()
export class SDKModels {
  private models: Models = {
    Player: Player,
    Rule: Rule,
    Game: Game
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
