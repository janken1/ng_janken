/* tslint:disable */

declare var Object: any;
export interface RuleInterface {
  id: number;
  move: string;
  kills: string;
  editable?: boolean;
}

export class Rule implements RuleInterface {
  'id': number;
  'move': string;
  'kills': string;
  'editable': boolean;
  constructor(data?: RuleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Rule`.
   */
  public static getModelName() {
    return 'Rule';
  }
  /**
   * @method factory
   * @author Jonathan Casarrubias
   * @license MIT
   * This method creates an instance of Rule for dynamic purposes.
   **/
  public static factory(data: RuleInterface): Rule {
    return new Rule(data);
  }
  /**
   * @method getModelDefinition
   * @author Julien Ledun
   * @license MIT
   * This method returns an object that represents some of the model
   * definitions.
   **/
  public static getModelDefinition() {
    return {
      name: 'Rule',
      plural: 'Rules',
      path: 'Rules',
      idName: 'id',
      properties: {
        id: {
          name: 'id',
          type: 'number'
        },
        move: {
          name: 'move',
          type: 'string'
        },
        kills: {
          name: 'kills',
          type: 'string'
        },
        editable: {
          name: 'editable',
          type: 'boolean',
          default: true
        }
      },
      relations: {}
    };
  }
}
