import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  NgZone
} from '@angular/core';
import { ROUTE_ANIMATIONS_ELEMENTS } from '../core/core.module';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { JankenService } from '../shared/janken-api/janken.service';
import {
  PlayerApi,
  LoopBackFilter,
  Player,
  Game,
  GameApi,
  Rule,
  RuleApi
} from '../shared/sdk';
import { Observable } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'anms-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  releaseButler = require('../../assets/release-butler.png');

  form = this.fb.group({
    player1: ['', [Validators.required]],
    player2: ['', [Validators.required]]
  });

  public loading = false;

  player1 = null;
  play1 = null;
  play2 = null;
  player2 = null;

  current_game = null;
  current_round = null;

  default_game_sequence = {
    rounds: [
      {
        player1: null,
        player2: null,
        winner: null
      }
    ]
  };

  rules: Rule[] = [];

  game_ended = false;

  constructor(
    private fb: FormBuilder,
    private jankenApi: JankenService,
    private playerApi: PlayerApi,
    private ngZone: NgZone,
    private gameApi: GameApi,
    private ruleApi: RuleApi,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    // this.jankenApi.setValue('current_game', null);
    console.log('current_game: ', this.jankenApi.getValue('current_game'));
    this.current_game = this.jankenApi.getValue('current_game');
    this.player1 = this.jankenApi.getValue('player1');
    this.player2 = this.jankenApi.getValue('player2');

    if (this.current_game) {
      this.current_game.game_sequence = this.current_game.game_sequence
        ? this.current_game.game_sequence
        : this.default_game_sequence;
      this.current_round = this.getCurrentRound(
        this.current_game.game_sequence
      );
    }

    this.ruleApi.find().subscribe((rules: Rule[]) => {
      this.rules = rules;
    });
  }

  start() {
    this.loading = true;
    const player1_name = this.form.get('player1').value;
    let player2_name = this.form.get('player2').value;
    if (player1_name === player2_name) {
      player2_name += Math.floor(Math.random() * 100) + 1;
    }
    const player_search = [];
    player_search.push(player1_name);
    player_search.push(player2_name);

    console.log('player2_name: ', player2_name);

    const lb_filter_player: LoopBackFilter = {
      where: {
        name: {
          inq: player_search
        }
      }
    };

    this.playerApi.find(lb_filter_player).subscribe(
      (players: Player[]) => {
        console.log('players: ', players);
        let player1 = null;
        let player2 = null;
        players.forEach(player => {
          if (player.name === player1_name) {
            player1 = player;
            this.jankenApi.setValue('player1', player1);
          }

          if (player.name === player2_name) {
            player2 = player;
            this.jankenApi.setValue('player2', player2);
          }
        });

        this.savePlayer(player1, player1_name, 'player1').subscribe(
          player1_saved => {
            this.player1 = player1_saved;

            this.savePlayer(player2, player2_name, 'player2').subscribe(
              player2_saved => {
                this.player2 = player2_saved;

                //  console.log('finaliza...', this.loading);

                this.createGame(this.player1, this.player2).subscribe(
                  game_created => {
                    this.jankenApi.setValue('current_game', game_created);
                    this.current_game = game_created;
                    this.loading = false;
                    this.current_round = this.getCurrentRound(
                      this.current_game.game_sequence
                    );
                  }
                );
              }
            );
          }
        );
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  createGame(player1: Player, player2: Player) {
    const observable: Observable<Game> = new Observable(observer => {
      const new_game = new Game();
      new_game.id = 0;
      new_game.created_it = moment().toDate();
      new_game.player1_id = player1.id;
      new_game.player2_id = player2.id;
      new_game.game_sequence = JSON.parse(
        JSON.stringify(this.default_game_sequence)
      );
      new_game.status = 'started';

      this.gameApi.create(new_game).subscribe(
        game_created => {
          observer.next(game_created);
          observer.complete();
        },
        error => {
          console.log(error);
          observer.next(null);
          observer.complete();
        }
      );
    });

    return observable;
  }

  savePlayer(player, player_name, key) {
    console.log(player);
    const observable: Observable<Player> = new Observable(observer => {
      if (!player) {
        const new_player = new Player();
        new_player.id = 0;
        new_player.name = player_name;

        this.playerApi
          .create(new_player)
          .subscribe((player_created: Player) => {
            this.jankenApi.setValue(key, player_created);
            observer.next(player_created);
            observer.complete();
          });
      } else {
        observer.next(player);
        observer.complete();
      }
    });

    return observable;
  }

  getCurrentRound(game_sequence) {
    return JSON.parse(
      JSON.stringify(game_sequence.rounds[game_sequence.rounds.length - 1])
    );
  }

  getCurrentRoundNumber(game_sequence) {
    return game_sequence.rounds.length;
  }

  setCurrentPlay(player, play) {
    this.loading = true;
    this.current_round[player] = play;
    if (this.current_round.player1 && this.current_round.player2) {
      const rule_play1 = this.rules.find(
        rule => rule.move === this.current_round.player1
      );
      const rule_play2 = this.rules.find(
        rule => rule.move === this.current_round.player2
      );

      if (rule_play1 !== undefined && rule_play2 !== undefined) {
        if (rule_play1.kills === rule_play2.move) {
          this.current_round.winner = this.player1.id;
        }

        if (rule_play2.kills === rule_play1.move) {
          this.current_round.winner = this.player2.id;
        }

        this.current_round.winner =
          this.current_round.winner === null ? 0 : this.current_round.winner;
      }
    }

    this.current_game.game_sequence.rounds[
      this.current_game.game_sequence.rounds.length - 1
    ] = JSON.parse(JSON.stringify(this.current_round));

    let game_ended = false;
    if (this.current_round.winner !== null) {
      const player1_winner = this.current_game.game_sequence.rounds.filter(
        round => round.winner === this.player1.id
      ).length;
      const player2_winner = this.current_game.game_sequence.rounds.filter(
        round => round.winner === this.player2.id
      ).length;

      if (player1_winner > 1 || player2_winner > 1) {
        game_ended = true;
      } else {
        this.initRound();
      }
    }

    if (game_ended) {
      this.current_game.status = 'ended';
    }

    this.gameApi.upsert(this.current_game).subscribe((game_updated: Game) => {
      this.jankenApi.setValue('current_game', game_updated);
      this.current_game = game_updated;
      this.loading = false;

      this.game_ended = game_ended;
    });
  }

  initRound() {
    this.current_game.game_sequence.rounds.push({
      player1: null,
      player2: null,
      winner: null
    });

    this.play1 = null;
    this.play2 = null;

    const tmp_round = this.getCurrentRound(this.current_game.game_sequence);

    this.current_round = JSON.parse(JSON.stringify(tmp_round));
  }

  getPlayerWinner(current_game) {
    const player1_winner = current_game.game_sequence.rounds.filter(
      round => round.winner === this.player1.id
    ).length;
    const player2_winner = current_game.game_sequence.rounds.filter(
      round => round.winner === this.player2.id
    ).length;
    return player1_winner > player2_winner
      ? this.player1.name
      : this.player2.name;
  }

  getPlayerRoundWinner(round) {
    return round.winner === this.player1.id
      ? this.player1.name
      : round.winner === this.player2.id
      ? this.player2.name
      : this.translate.instant('janken.pages.step2.subtitle.tied');
  }

  newGame() {
    this.player1 = null;
    this.play1 = null;
    this.play2 = null;
    this.player2 = null;

    this.current_game = null;
    this.current_round = null;
    this.jankenApi.setValue('current_game', null);
  }
}
