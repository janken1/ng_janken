import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [PagesComponent, SettingsComponent],
  imports: [
    CommonModule,
    SharedModule,
    PagesRoutingModule,
    NgxLoadingModule.forRoot({})
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagesModule {}
