import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Rule, RuleApi } from '../../shared/sdk';
import { FormBuilder, NgForm } from '@angular/forms';
import { ROUTE_ANIMATIONS_ELEMENTS } from '../../core/core.module';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'anms-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

  rules: Rule[] = [];

  ruleFormGroup = this.fb.group(SettingsComponent.createRule());

  loading = false;
  isEditing: boolean;
  rule_selected: Rule = null;
  static createRule(): Rule {
    return {
      id: 0,
      move: '',
      kills: '',
      editable: true
    };
  }

  constructor(
    public fb: FormBuilder,
    private ruleApi: RuleApi,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loading = true;
    this.loadRules().subscribe(rules => {
      this.rules = rules;
      this.loading = false;
    });
  }
  loadRules() {
    const observable: Observable<Rule[]> = new Observable(observer => {
      this.ruleApi.find().subscribe((rules: Rule[]) => {
        observer.next(rules);
        observer.complete();
      });
    });

    return observable;
  }

  select(rule: Rule) {
    this.isEditing = false;
    this.rule_selected = rule;
  }
  deselect() {
    this.isEditing = false;
    this.rule_selected = null;
  }

  addNew(ruleForm: NgForm) {
    ruleForm.resetForm();
    this.ruleFormGroup.reset();
    this.ruleFormGroup.setValue(SettingsComponent.createRule());
    this.isEditing = true;
    this.rule_selected = null;
  }

  edit(rule: Rule) {
    if (rule.editable) {
      this.isEditing = true;
      this.ruleFormGroup.setValue(rule);
    }
  }

  delete(rule: Rule) {
    if (rule.editable) {
      this.loading = true;
      this.ruleApi.deleteById(rule.id).subscribe(resp => {
        this.deselect();
        this.loadRules().subscribe(rules => {
          this.rules = rules;
          this.loading = false;
        });
      });
    }
  }

  save() {
    if (this.ruleFormGroup.valid) {
      this.loading = true;
      console.log('Válido...');
      let is_new = true;
      let add_record = false;
      const move = this.ruleFormGroup.get('move').value;
      const kills = this.ruleFormGroup.get('kills').value;
      this.rules.forEach(rule => {
        if (rule.move === move && rule.kills === kills) {
          if (this.rule_selected) {
            if (this.rule_selected.id === rule.id) {
              is_new = false;
              add_record = true;
            } else {
              alert(
                this.translate.instant('janken.menu.settings.rules.crud.exist')
              );
            }
          } else {
            alert(
              this.translate.instant('janken.menu.settings.rules.crud.exist')
            );
          }
        } else if (rule.move === kills && rule.kills === move) {
          alert(
            this.translate.instant('janken.menu.settings.rules.crud.exist2')
          );
        } else {
          add_record = true;
        }
      });
      if (add_record) {
        const rule_record = is_new
          ? new Rule(SettingsComponent.createRule())
          : this.rule_selected;

        rule_record.move = move;
        rule_record.kills = kills;

        const function_rule = is_new
          ? this.ruleApi.create(rule_record)
          : this.ruleApi.upsert(rule_record);

        function_rule.subscribe(rule_saved => {
          this.deselect();
          this.loadRules().subscribe(rules => {
            this.rules = rules;
            this.loading = false;
          });
        });
      } else {
        this.loading = false;
      }
    }
  }
}
